<?php
declare(strict_types=1);

namespace ClickHouse\Eloquent;

use Illuminate\Database\Eloquent\Model as BaseModel;

/**
 * Class Model
 * @package ClickHouse\Eloquent
 */
abstract class Model extends BaseModel
{

}
