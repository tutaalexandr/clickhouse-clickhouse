<?php
declare(strict_types=1);

namespace ClickHouse\Query;

use Illuminate\Database\Query\Builder as BaseBuilder;

/**
 * Class Builder
 * @package ClickHouse\Query
 */
class Builder extends BaseBuilder
{

}
