<?php
declare(strict_types=1);

namespace ClickHouse\Query;

use Illuminate\Database\Query\Grammars\MySqlGrammar as BaseGrammar;

/**
 * Class Grammar
 * @package ClickHouse\Query
 */
class Grammar extends BaseGrammar
{

}
