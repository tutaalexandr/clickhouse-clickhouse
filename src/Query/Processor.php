<?php
declare(strict_types=1);

namespace ClickHouse\Query;

use Illuminate\Database\Query\Processors\MySqlProcessor as BaseProcessor;

/**
 * Class Processor
 * @package ClickHouse\Query
 */
class Processor extends BaseProcessor
{

}
