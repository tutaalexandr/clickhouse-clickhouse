<?php
declare(strict_types=1);

namespace ClickHouse\Schema;

use Illuminate\Database\Schema\Grammars\MySqlGrammar as BaseGrammar;

/**
 * Class Grammar
 * @package ClickHouse\Schema
 */
class Grammar extends BaseGrammar
{

}
