<?php
declare(strict_types=1);

namespace ClickHouse\Schema;

use Illuminate\Database\Schema\MySqlBuilder as BaseBuilder;

/**
 * Class Builder
 * @package ClickHouse\Schema
 */
class Builder extends BaseBuilder
{

}
