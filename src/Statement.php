<?php
declare(strict_types=1);

namespace ClickHouse;

use ClickHouseDB\Client;

/**
 * Class Statement
 * @package ClickHouse
 */
class Statement
{
    /** @var Client */
    protected $client;

    /**
     * @var string
     */
    protected $statement;

    protected $rows = [];
    protected $values = [];
    protected $types = [];

    /** @var \ArrayIterator|null */
    protected $iterator;

    private $fetchMode = Driver::FETCH_BOTH;

    /**
     * Statement constructor.
     * @param Client $client
     * @param string $statement
     */
    public function __construct($client, $statement)
    {
        $this->client = $client;
        $this->statement = $statement;
    }

    /**
     * @param $mode
     * @param null $classNameObject
     * @param array $ctorarfg
     * @return bool
     */
    public function setFetchMode($mode, $classNameObject = null, array $ctorarfg = array())
    {
        $this->fetchMode = $this->assumeFetchMode($mode);
        return true;
    }

    /**
     * @param $parameter
     * @param $value
     * @param int $data_type
     */
    public function bindValue($parameter, $value, $data_type = Driver::PARAM_STR): void
    {
        $this->values[$parameter] = $value;
        $this->types[$parameter] = $data_type;
    }

    /**
     * @param null $input_parameters
     * @return bool
     */
    public function execute ($input_parameters = null): bool
    {
        $hasZeroIndex = false;
        if (is_array($input_parameters)) {
            $this->values = array_replace($this->values, $input_parameters);//TODO array keys must be all strings or all integers?
            $hasZeroIndex = array_key_exists(0, $input_parameters);
        }

        $sql = $this->statement;

        if ($hasZeroIndex) {
            $statementParts = explode('?', $sql);
            array_walk($statementParts, function (&$part, $key) : void {
                if (! array_key_exists($key, $this->values)) {
                    return;
                }

                $part .= $this->getTypedParam($key);
            });
            $sql = implode('', $statementParts);
        } else {
            foreach (array_keys($this->values) as $key) {
                $sql = preg_replace(
                    '/(' . (is_int($key) ? '\?' : ':' . $key) . ')/i',
                    $this->getTypedParam($key),
                    $sql,
                    1
                );
            }
        }

        $this->processViaClient($sql);

        return true;
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator() : \ArrayIterator
    {
        if (! $this->iterator) {
            $this->iterator = new \ArrayIterator($this->rows);
        }

        return $this->iterator;
    }

    /**
     * @param null $fetch_style
     * @param int $cursor_orientation
     * @param int $cursor_offset
     * @return array|bool|mixed|object
     */
    public function fetch ($fetch_style = null, $cursor_orientation = Driver::FETCH_ORI_NEXT, $cursor_offset = 0)
    {
        $data = $this->getIterator()->current();

        if ($data === null) {
            return false;
        }

        $this->getIterator()->next();

        if ($this->assumeFetchMode($fetch_style) === Driver::FETCH_NUM) {
            return array_values($data);
        }

        if ($this->assumeFetchMode($fetch_style) === Driver::FETCH_BOTH) {
            return array_values($data) + $data;
        }

        if ($this->assumeFetchMode($fetch_style) === Driver::FETCH_OBJ) {
            return (object) $data;
        }

        if ($this->assumeFetchMode($fetch_style) === Driver::FETCH_KEY_PAIR) {
            if (count($data) < 2) {
                throw new \RuntimeException(
                    'To fetch in \PDO::FETCH_KEY_PAIR mode, result set must contain at least 2 columns'
                );
            }

            return [array_shift($data) => array_shift($data)];
        }

        return $data;
    }

    /**
     * @param null $fetch_style
     * @param null $fetch_argument
     * @param array $ctor_args
     * @return array
     */
    public function fetchAll ($fetch_style = null, $fetch_argument = null, array $ctor_args = array())
    {
        if ($this->assumeFetchMode($fetch_style) === Driver::FETCH_NUM) {
            return array_map(
                'array_values',
                $this->rows
            );
        }

        if ($this->assumeFetchMode($fetch_style) === Driver::FETCH_BOTH) {
            return array_map(
                function ($row) {
                    return array_values($row) + $row;
                },
                $this->rows
            );
        }

        if ($this->assumeFetchMode($fetch_style) === Driver::FETCH_OBJ) {
            return array_map(
                function ($row) {
                    return (object) $row;
                },
                $this->rows
            );
        }

        if ($this->assumeFetchMode($fetch_style) === Driver::FETCH_KEY_PAIR) {
            return array_map(
                function ($row) {
                    if (count($row) < 2) {
                        throw new \RuntimeException(
                            'To fetch in \PDO::FETCH_KEY_PAIR mode, result set must contain at least 2 columns'
                        );
                    }

                    return [array_shift($row) => array_shift($row)];
                },
                $this->rows
            );
        }

        return $this->rows;
    }

    /**
     * @param string $sql
     */
    protected function processViaClient(string $sql) : void
    {
        $sql = trim($sql);

        $this->rows =
            stripos($sql, 'select') === 0 ||
            stripos($sql, 'show') === 0 ||
            stripos($sql, 'describe') === 0 ?
                $this->client->select($sql)->rows() :
                $this->client->write($sql)->rows();
    }

    /**
     * @param int|null $fetchMode
     * @return int
     */
    protected function assumeFetchMode(?int $fetchMode = null) : int
    {
        $mode = $fetchMode ?: $this->fetchMode;
        if (! in_array($mode, [
            Driver::FETCH_ASSOC,
            Driver::FETCH_NUM,
            Driver::FETCH_OBJ,
            Driver::FETCH_KEY_PAIR,
        ], true)) {
            $mode = Driver::FETCH_BOTH;
        }

        return $mode;
    }

    /**
     * @param $key
     * @return string
     */
    protected function getTypedParam($key) : string
    {
        if ($this->values[$key] === null) {
            return 'NULL';
        }

        $type = $this->types[$key] ?? null;

        // if param type was not setted - trying to get db-type by php-var-type
        if ($type === null) {
            if (is_bool($this->values[$key])) {
                $type = Driver::PARAM_BOOL;
            } elseif (is_int($this->values[$key]) || is_float($this->values[$key])) {
                $type = Driver::PARAM_INT;
            } elseif (is_array($this->values[$key])) {
                /*
                 * ClickHouse Arrays
                 */
                $values = $this->values[$key];
                if (is_int(current($values)) || is_float(current($values))) {
                    array_map(
                        function ($value) : void {
                            if (! is_int($value) && ! is_float($value)) {
                                throw new \RuntimeException(
                                    'Array values must all be int/float or string, mixes not allowed'
                                );
                            }
                        },
                        $values
                    );
                } else {
                    $values = array_map(function ($value) {
                        return $value === null ? 'NULL' : Driver::quoteStringLiteral($value);
                    }, $values);
                }

                return '[' . implode(', ', $values) . ']';
            }
        }

        if ($type === Driver::PARAM_INT) {
            return (string) $this->values[$key];
        }

        if ($type === Driver::PARAM_BOOL) {
            return (string) (int) (bool) $this->values[$key];
        }

        return Driver::quoteStringLiteral((string) $this->values[$key]);
    }
}
