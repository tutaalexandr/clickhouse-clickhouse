<?php
declare(strict_types=1);

namespace ClickHouse;

use ClickHouse\Events\StatementPrepared;
use Illuminate\Database\Connection as BaseConnection;

/**
 * Class Connection
 * @package ClickHouse
 */
class Connection extends BaseConnection
{
    /**
     * @var Driver $driver
     */
    protected $driver;

    /**
     * @inheritDoc
     */
    public function __construct($pdo, $database = '', $tablePrefix = '', array $config = [])
    {
        parent::__construct($pdo, $database, $tablePrefix, $config);

        $this->driver = $this->createDriver();
    }

    /**
     * @return Driver
     */
    protected function createDriver()
    {
        return new Driver($this->config);
    }

    /**
     * @inheritDoc
     */
    public function select($query, $bindings = [], $useReadPdo = true)
    {
        return $this->run($query, $bindings, function ($query, $bindings) use ($useReadPdo) {
            if ($this->pretending()) {
                return [];
            }

            // For select statements, we'll simply execute the query and return an array
            // of the database result set. Each element in the array will be a single
            // row from the database table, and will either be an array or objects.
            $statement = $this->nonPdoPrepared($this->getPdoForSelect($useReadPdo)
                ->prepare($query));

            $this->nonPdoBindValues($statement, $this->prepareBindings($bindings));

            $statement->execute();

            return $statement->fetchAll();
        });
    }

    /**
     * @inheritDoc
     */
    public function statement($query, $bindings = [])
    {
        return $this->run($query, $bindings, function ($query, $bindings) {
            if ($this->pretending()) {
                return true;
            }

            $statement = $this->getPdo()->prepare($query);

            $this->nonPdoBindValues($statement, $this->prepareBindings($bindings));

            $this->recordsHaveBeenModified();

            return $statement->execute();
        });
    }

    /**
     * Configure the PDO prepared statement.
     *
     * @param  Statement  $statement
     * @return Statement
     */
    protected function nonPdoPrepared(Statement $statement)
    {
        $statement->setFetchMode($this->fetchMode);
        $this->event(new StatementPrepared($this, $statement));
        return $statement;
    }

    /**
     * @param Statement $statement
     * @param $bindings
     */
    public function nonPdoBindValues($statement, $bindings)
    {
        foreach ($bindings as $key => $value) {
            $statement->bindValue(
                is_string($key) ? $key : $key + 1, $value,
                is_int($value) ? Driver::PARAM_INT : Driver::PARAM_STR
            );
        }
    }

    /**
     * @inheritDoc
     */
    public function cursor($query, $bindings = [], $useReadPdo = true)
    {
        $statement = $this->run($query, $bindings, function ($query, $bindings) use ($useReadPdo) {
            if ($this->pretending()) {
                return [];
            }

            // First we will create a statement for the query. Then, we will set the fetch
            // mode and prepare the bindings for the query. Once that's done we will be
            // ready to execute the query against the database and return the cursor.
            $statement = $this->nonPdoPrepared($this->getPdoForSelect($useReadPdo)
                ->prepare($query));

            $this->nonPdoBindValues(
                $statement, $this->prepareBindings($bindings)
            );

            // Next, we'll execute the query against the database and return the statement
            // so we can return the cursor. The cursor will use a PHP generator to give
            // back one row at a time without using a bunch of memory to render them.
            $statement->execute();

            return $statement;
        });

        while ($record = $statement->fetch()) {
            yield $record;
        }
    }

    /**
     * @inheritDoc
     */
    public function getPdo()
    {
        return $this->driver;
    }

    /**
     * @inheritDoc
     */
    protected function getPdoForSelect($useReadPdo = true)
    {
        return $this->driver;
    }

    /**
     * @inheritDoc
     */
    public function getSchemaBuilder()
    {
        if ($this->schemaGrammar === null) {
            $this->useDefaultSchemaGrammar();
        }

        return new Schema\Builder($this);
    }

    /**
     * @inheritDoc
     */
    public function getDatabaseName()
    {
        return $this->driver->settings()->getDatabase();
    }

    /**
     * @inheritDoc
     */
    public function disconnect()
    {
        unset($this->driver);
    }

    /**
     * @inheritDoc
     */
    public function getDriverName()
    {
        return 'clickhouse';
    }

    /**
     * @inheritDoc
     */
    public function query()
    {
        return new Query\Builder($this, $this->getQueryGrammar(), $this->getPostProcessor());
    }

    /**
     * @inheritDoc
     */
    protected function getDefaultPostProcessor()
    {
        return new Query\Processor();
    }

    /**
     * @inheritDoc
     */
    protected function getDefaultQueryGrammar()
    {
        return new Query\Grammar();
    }

    /**
     * @inheritDoc
     */
    protected function getDefaultSchemaGrammar()
    {
        return new Schema\Grammar();
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->driver, $name], $arguments);
    }
}
