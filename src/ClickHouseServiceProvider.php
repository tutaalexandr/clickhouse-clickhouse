<?php
declare(strict_types=1);

namespace ClickHouse;

use ClickHouse\Eloquent\Model;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\ServiceProvider;

/**
 * Class ClickHouseServiceProvider
 * @package ClickHouse
 */
class ClickHouseServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function boot()
    {
        Model::setConnectionResolver($this->app['db']);
        Model::setEventDispatcher($this->app['events']);
    }

    /**
     *
     */
    public function register()
    {
        $this->app->resolving('db', function ($db) {
            /**@var DatabaseManager $db*/
            $db->extend('clickhouse', function ($config, $name) {
                $config['name'] = $name;
                return new Connection(false, $config['database'], $config['prefix'], $config);
            });
        });
    }
}
