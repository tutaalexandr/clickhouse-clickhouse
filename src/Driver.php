<?php
declare(strict_types=1);

namespace ClickHouse;

use ClickHouseDB\Client;
use ClickHouseDB\Settings;

/**
 * Class Driver
 * @package ClickHouse
 */
class Driver
{
    public const ATTR_AUTOCOMMIT = 0;
    public const ATTR_TIMEOUT = 2;
    public const ATTR_ERRMODE = 3;
    public const ATTR_CASE = 8;
    public const ATTR_ORACLE_NULLS = 11;
    public const ATTR_STATEMENT_CLASS = 13;
    public const ATTR_STRINGIFY_FETCHES = 17;
    public const ATTR_DEFAULT_FETCH_MODE = 19;
    public const ATTR_EMULATE_PREPARES = 20;
    public const MYSQL_ATTR_USE_BUFFERED_QUERY = 1000;

    // ATTR_CASE
    public const CASE_NATURAL = 0;
    public const CASE_UPPER = 1;
    public const CASE_LOWER = 2;

    // ATTR_ERRMODE
    public const ERRMODE_SILENT = 0;
    public const ERRMODE_WARNING = 1;
    public const ERRMODE_EXCEPTION = 2;

    // ATTR_ORACLE_NULLS
    public const NULL_NATURAL = 0;
    public const NULL_EMPTY_STRING = 1;
    public const NULL_TO_STRING = 2;

    public const MAP_ATTR = [
        self::ATTR_TIMEOUT => 'max_execution_time',
    ];

    public const PARAM_NULL = 0;
    public const PARAM_INT = 1;
    public const PARAM_STR = 2;
    public const PARAM_LOB = 3;
    public const PARAM_STMT = 4;
    public const PARAM_BOOL = 5;

    public const FETCH_LAZY = 1;
    public const FETCH_ASSOC = 2;
    public const FETCH_NUM = 3;
    public const FETCH_BOTH = 4;
    public const FETCH_OBJ = 5;
    public const FETCH_BOUND = 6;
    public const FETCH_COLUMN = 7;
    public const FETCH_CLASS = 8;
    public const FETCH_INTO = 9;
    public const FETCH_FUNC = 10;
    public const FETCH_NAMED = 11;
    public const FETCH_KEY_PAIR = 12;
    public const FETCH_GROUP = 65536;
    public const FETCH_UNIQUE = 196608;
    public const FETCH_CLASSTYPE = 262144;
    public const FETCH_PROPS_LATE = 1048576;

    public const FETCH_ORI_NEXT = 0;
    public const FETCH_ORI_PRIOR = 1;
    public const FETCH_ORI_FIRST = 2;
    public const FETCH_ORI_LAST = 3;
    public const FETCH_ORI_ABS = 4;
    public const FETCH_ORI_REL = 5;

    protected $config = [];

    /**
     * @var Client
     */
    protected $client;

    /**
     * Driver constructor.
     * @param array $config
     */
    public function __construct($config)
    {
        $this->config = $config;
        $this->client = $this->createClient();
    }

    /**
     * @return Client
     */
    protected function createClient(): Client
    {
        return new Client(
            [
                'host' => $this->config['host'],
                'port' => $this->config['port'],
                'username' => $this->config['username'],
                'password' => $this->config['password'],
                'readonly' => $this->config['readonly'],
                'https' => $this->config['https'],
            ],
            array_merge(
                [
                    'database' => $this->config['database'],
                ],
                $this->config['options'] ?? []
            )
        );
    }

    /**
     * @return Settings
     */
    public function settings(): Settings
    {
        return $this->client->settings();
    }

    /**
     * @return string
     */
    public static function getStringLiteralQuoteCharacter()
    {
        return "'";
    }

    /**
     * @param $str
     * @return string
     */
    public static  function quoteStringLiteral($str)
    {
        $c = self::getStringLiteralQuoteCharacter();
        return $c . str_replace($c, $c . $c, $str) . $c;
    }

    /**
     * @param $string
     * @param int $parameter_type
     * @return string
     */
    public function quote($string, $parameter_type = self::PARAM_STR)
    {
        if ($parameter_type === self::PARAM_INT) {
            return $string;
        }

        return self::quoteStringLiteral($string);
    }

    /**
     * @param string $stmt
     * @param $driver_options array
     * @return Statement
     */
    public function prepare($statement, array $driver_options = [])
    {
        return new Statement($this->client, $statement);
    }

    /**
     * @param int $attribute
     * @return mixed
     */
    public function getAttribute($attribute)
    {
        if ($this->client->settings()->is($attribute)) {
            return $this->client->settings()->get($attribute);
        }

        return null;
    }

    /**
     * @param int $attribute
     * @param mixed $value
     * @return bool
     */
    public function setAttribute($attribute, $value)
    {
        if (isset(self::MAP_ATTR[$attribute])) {
            $attribute = self::MAP_ATTR[$attribute];
        }

        $this->client->settings()->set($attribute, $value);
        return true;
    }
}
